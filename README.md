#Mautic Api Symfony Bundle

##Requerimientos

- [Symfony 2.8+]
- [mautic/api-library](github.com/mautic/api-library)

##Instalación

    composer require tmwk/mautic_api_bundle
    
Modifica el archivo app/AppKernel.php para registrar el nuevo Bundle
    
    $bundles = array(
        // ...
        new Tmwk\MauticApiBundle\TmwkMauticApiBundle(),,
        // ...
    );
    
Agrega a tu archivo app/config/config.yml lo siguiente:

    tmwk_mautic_api:
        url: "%mautic_api_url%"
        user: "%mautic_api_user%"
        pass: "%mautic_api_pass%"
        
Agrega a tu archivo app/config/parameters.yml lo siguiente:

    mautic_api_url: <inserta el url api mautic (https://misitio.com/api)>
    mautic_api_user: <Ingrese el nombre de usuario admin>
    mautic_api_pass: <Ingrese la contraseña del usuario>

           

##Uso

###Ejemplo 

Lista las campañas disponibles

    $spp = $this->get('tmwk.mautic_api');
    
    $campains = $mautic_api->campaigns();

    $campaignApi = $campains->getList();

    echo '<pre>';
    print_r($campaignApi);
    echo '</pre>';
