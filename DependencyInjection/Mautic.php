<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 14/03/2017
 * Time: 22:34
 */

namespace Tmwk\MauticApiBundle\DependencyInjection;

use Mautic\Auth\ApiAuth;
use Mautic\MauticApi;

class Mautic
{
    protected static $apiUrl;
    protected static $user;
    protected static $pass;

    public function __construct($user, $pass, $apiUrl)
    {
        self::$apiUrl = $apiUrl;
        self::$user = $user;
        self::$pass = $pass;
    }

    public static function auth()
    {
        $settings = array(
            'AuthMethod' => 'BasicAuth',
            'userName'   => self::$user,
            'password'   => self::$pass,
            'apiUrl'     => self::$apiUrl,
        );

        $initAuth = new ApiAuth();
        $auth = $initAuth->newAuth($settings, $settings['AuthMethod']);

        if ($auth->isAuthorized()) {
            return $auth;
        } else {
            return false;
        }
    }

    /**
     * @return \Mautic\Api\Assets
     */
    public function assets()
    {
        $api = new MauticApi();
        return $api->newApi("assets", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Campaigns
     */
    public function campaigns()
    {
        $api = new MauticApi();
        return $api->newApi("campaigns", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Categories
     */
    public function categories()
    {
        $api = new MauticApi();
        return $api->newApi("categories", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Companies
     */
    public function companies()
    {
        $api = new MauticApi();
        return $api->newApi("companies", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Contacts
     */
    public function contacts()
    {
        $api = new MauticApi();
        return $api->newApi("contacts", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Data
     */
    public function data()
    {
        $api = new MauticApi();
        return $api->newApi("data", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\DynamicContents
     */
    public function dynamicConents()
    {
        $api = new MauticApi();
        return $api->newApi("dynamicConents", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\CompanyFields
     */
    public function companyFields()
    {
        $api = new MauticApi();
        return $api->newApi("companyFields", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Emails
     */
    public function emails()
    {
        $api = new MauticApi();
        return $api->newApi("emails", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Files
     */
    public function files()
    {
        $api = new MauticApi();
        return $api->newApi("files", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Forms
     */
    public function forms()
    {
        $api = new MauticApi();
        return $api->newApi("forms", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Notes
     */
    public function notes()
    {
        $api = new MauticApi();
        return $api->newApi("notes", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Notifications
     */
    public function notifications()
    {
        $api = new MauticApi();
        return $api->newApi("notifications", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Pages
     */
    public function pages()
    {
        $api = new MauticApi();
        return $api->newApi("pages", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Points
     */
    public function points()
    {
        $api = new MauticApi();
        return $api->newApi("points", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\PointTriggers
     */
    public function pointTriggers()
    {
        $api = new MauticApi();
        return $api->newApi("pointTriggers", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Roles
     */
    public function roles()
    {
        $api = new MauticApi();
        return $api->newApi("roles", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Segments
     */
    public function segments()
    {
        $api = new MauticApi();
        return $api->newApi("segments", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Smses
     */
    public function smses()
    {
        $api = new MauticApi();
        return $api->newApi("smses", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Stages
     */
    public function stages()
    {
        $api = new MauticApi();
        return $api->newApi("stages", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Stats
     */
    public function stats()
    {
        $api = new MauticApi();
        return $api->newApi("stats", self::auth(), self::$apiUrl);
    }

    /**
     * @return \Mautic\Api\Users
     */
    public function users()
    {
        $api = new MauticApi();
        return $api->newApi("users", self::auth(), self::$apiUrl);
    }


}